/**
 * Created by Roni on 26/11/2016.
 */


jQuery(document).ready(function($) {
    $(".scroll").click(function(event){
        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
    });
    $('#slider').rbtSlider({
  	  
  	  height: '100vh', 
  	  'dots': true,
  	  'arrows': true,
  	  'auto': 3
  	});
    
    $('.jcarousel').jcarousel({
        // Configuration goes here
    });
    
});
$(document).ready(function() {
    $('#fullpage').fullpage({
        'verticalCentered': false,
        'css3': true,
        'sectionsColor': ['#F0F2F4', '#fff', '#fff', '#fff'],
        'navigation': true,
        'navigationPosition': 'right',
        'navigationTooltips': ['Home', 'Protfolio', 'Contact', 'Simple'],

        'afterLoad': function(anchorLink, index){
            if(index == 2){
                $('#iphone3, #iphone2, #iphone4').addClass('active');
            }
        },

        'onLeave': function(index, nextIndex, direction){
            if (index == 3 && direction == 'down'){
                $('.section').eq(index -1).removeClass('moveDown').addClass('moveUp');
            }
            else if(index == 3 && direction == 'up'){
                $('.section').eq(index -1).removeClass('moveUp').addClass('moveDown');
            }

            $('#staticImg').toggleClass('active', (index == 2 && direction == 'down' ) || (index == 4 && direction == 'up'));
            $('#staticImg').toggleClass('moveDown', nextIndex == 4);
            $('#staticImg').toggleClass('moveUp', index == 4 && direction == 'up');
        }
    });
});

jQuery(document).ready(function($) {

    $('#submitBtn').click(function(){
        $.post("mail.php",{
            "user_name":$("[name='user_name']").val(),
            "user_mail":$("[name='user_mail']").val(),
            "user_message":$("[name='user_message']").val()
        },function(res){
            if (res == 'error')
                alert("problem with details!");
            else
                alert("Thanks!");
        });
    });



    $('#arrowlink').click(function(){
        $('html, body').animate({
            scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top
        }, 1000);
        return false;
    });

    $('#contactLink').click(function(){
        $.fn.fullpage.moveTo(3);
    });
});

$(window).load(function() {
    $("#flexiselDemo1").flexisel({
        visibleItems: 4,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 3000,
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint:480,
                visibleItems: 1
            },
            landscape: {
                changePoint:640,
                visibleItems: 2
            },
            tablet: {
                changePoint:768,
                visibleItems: 3
            }
        }
    });

});
